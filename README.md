# Worky

Construye un Giphy client que busque perritos, de preferencia en Vue.js
Responsivo.

Utiliza componentes reutilizables.

Maneja animaciones que le den ganas al usuario de regresar.

Guardar Mis búsquedas anteriores en localstorage.

Guardar Mis favoritos en localstorage.

Sube tu propuesta al repositorio como Merge Request.

Opcionalmente: Usa tailwind para definir los estilos de tu componentes.
Nota: Si utilizas Vue, debe ser en la versión 2.x

## Demo del proyecto

https://worky-test.netlify.app/

### Herramientas utilizadas

- Tailwind CSS
- Vue.js
- Giphy API
